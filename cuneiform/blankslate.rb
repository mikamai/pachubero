
# BlankSlate has been ripped from jim weirich work at onestepback.org
# and adapted
class BlankSlate
    instance_methods.each { |m| undef_method m unless m =~ /^__/ }
end
