require 'rexml/document'
require 'yaml'

require 'cuneiform/blankslate'
require 'cuneiform/rexml-extensions'
require 'cuneiform/string_implicit-typifycation'

def xml data
	XmlNode.new data
end

def xml_typification_on
    $xml_auto_typification = true
end

def xml_typification_off
    $xml_auto_typification = false
end

xml_typification_on

module FakeTextNode
  def t;  self.to_s   end
  def v;  self        end
end

class Numeric
  include FakeTextNode
end

class XmlNode < BlankSlate
    
	def initialize data
    @xml_node = rexml_node_from data
	end

	def method_missing name_symbol
    # puts "node.missing #{name_symbol}"
    name = name_symbol.to_s
        
		if has_attribute? name
			tipify @xml_node.attribute(name).value      
    elsif has_only_one_shallow_property? name
      tipify text_of_single_shallow_child_tagged name
		else
      xml_node_array_of_children_tagged_with name
		end
	end

  def to_s
    return @xml_node.text        
    return @xml_node.to_s(0)        
    return @xml_node.to_s        
  end
  
  def is_a? name
    if name.is_a? Symbol then
      name == self.xml_class
    else
      super
    end
  end
  
  def xml_class
    @xml_node.name.intern    
  end
  
  def s
    own_type = @xml_node.name.strip
    own_text = @xml_node.text.strip
    children = @xml_node.children.select{|c| c.is_a? REXML::Element }.map{|c| xml(c).s}.join ' '
    children = ' '+children if children
    
    "(#{own_type}:#{own_text}#{children})"
  end
  
  def t
   @xml_node.text || ''
  end
  
  def typified_content
    tipify self.t
  end
  
  def v
    tipify self.t
  end

  def typed
    v
  end
  
  def has? name_symbol
    name = name_symbol.to_s
    
    (has_attribute? name) or (has_tag? name)
  end

  def has_one_single? name_symbol
    name = name_symbol.to_s
    
    (has_attribute? name) or (has_one_single_tag? name)
  end

  def has_as_only_property? name_symbol
    name = name_symbol.to_s
    
    (has_attribute?   name) or 
    (has_as_only_tag? name)
  end

  def has_no_property?
		@xml_node.attributes.size.zero? and @xml_node.elements.size.zero?
	end

private
	def rexml_node_from data
        case data
    		when REXML::Node then data
		    when String      then REXML::Document.new(data).root
		    else raise "cannot interpret an XmlNode from a #{data.class}"
		end
	end
    
	def has_attribute? attr
		not @xml_node.attributes[attr].nil?		
	end

	def has_tag? name
		not @xml_node.elements[name].nil?		
	end
    
	def has_one_single_tag? name
        (has_tag? name) and ( (children_tagged_with name).size <= 1 )
    end
    
	def has_as_only_tag? name
        (has_tag? name) and (@xml_node.children.size <= 1)
  end
  
  def children_tagged_with tag
      @xml_node.children.select do |child| 
          (child.is_a? REXML::Element) and (child.name == tag)
      end            
  end

  def xml_node_array_of_children_tagged_with name
      XmlNodeArray.new( 
          (children_tagged_with name).map{|n| (xml n) } 
      )        
  end

  def is_shallow_tag? tag
      1 == (children_tagged_with tag).first.depth and
      (children_tagged_with tag).first.attributes.size.zero?               
  end    

  def is_shallow? property
     return true if has_attribute? property 

      is_shallow_tag? property
  end

  def is_only_child? tag
      1 == (children_tagged_with tag).size
  end
  
  def has_only_one_property? prop
      return true if has_attribute? prop        
      
      is_only_child? prop
  end
  
  def has_only_one_shallow_property? property
      (has_only_one_property? property) and (is_shallow? property) 
  end
  
  def has_many_shallow_property? property
      (not has_only_one_property? property) and (is_shallow? property) 
  end

  def has_single_shallow_tag? tag
      (is_only_child? tag) and (is_shallow? tag)
  end

  def text_of_single_shallow_child_tagged tag
      @xml_node.elements[tag].text        
  end

	def tipify string
    if $xml_auto_typification 
        result = string.to_implicit_type
    else
        result = string
    end
    
    result.extend FakeTextNode unless result.is_a? Fixnum
    
    return result
	end

end

class XmlNodeArray < BlankSlate
	
	def initialize nodes
		@nodes = nodes 
	end
	
	def [] key
		if key.is_a? Integer
			node = @nodes[key] 
		else
			node = child_with_id key
		end
        
    if node.has_no_property? then
        node.typified_content
    else
        node
    end
	end

  def size
    @nodes.size
  end
  
  def first
    self[0]    
  end
    
  def last
    self[-1]    
  end
  
  def each &block
    @nodes.each {|n| block.call n }        
  end
  
  def each_with_index &block
    @nodes.each_with_index {|n,i| block.call n,i }        
  end
  
  def map &block
    @nodes.map {|n| block.call n }        
  end
  
  def to_s
    self.first.to_s
  end  
  
  def respond_to? xxx
    false
  end
  
	def method_missing name, *args
    # puts "array.missing #{name}"
		self.first.__send__ name, *args
	end
	
private
	def child_with_id id
		matches = @nodes.select{|n| n.id == id}
        
		return nil if matches.empty?
		
    return matches.first
	end
end


