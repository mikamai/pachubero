module REXML

  class Parent
    def depth
        return 0 if not has_children?       
        
        1 + children.map{ |child| child.depth }.max
    end
    
    def has_children? 
        not self.size.zero?
    end
  end

  class Child
    def depth; 0 end
    def has_children?; false end 
  end

end
