MOCK_DATA = {
1202 => %{
<?xml version="1.0" encoding="UTF-8"?>
<eeml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.eeml.org/xsd/005" version="5" xsi:schemaLocation="http://www.eeml.org/xsd/005 http://www.eeml.org/xsd/005/005.xsd">
  <environment updated="2009-01-04T12:32:01Z" id="1202" creator="http://www.haque.co.uk">
    <title>Badgerpower</title>
    <feed>http://www.pachube.com/api/1202.xml</feed>
    <status>live</status>
    <description>Output from currentcost meter</description>
    <website>http://music.crouchingbadger.com/current</website>
    <location domain="physical" exposure="indoor" disposition="fixed">
      <name>OX4 4TN</name>
      <lat>51.7177447203256</lat>
      <lon>-1.22573090018705</lon>
    </location>
    <data id="0">
      <tag>power</tag>
      <value minValue="146.0" maxValue="7798.0">653</value>
    </data>
    <data id="1">
      <value minValue="10.5" maxValue="23.6">16.6</value>
    </data>
  </environment>
</eeml>
},
1203 => %{
<?xml version="1.0" encoding="UTF-8"?>
<eeml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.eeml.org/xsd/005" version="5" xsi:schemaLocation="http://www.eeml.org/xsd/005 http://www.eeml.org/xsd/005/005.xsd">
  <environment updated="2008-12-17T16:01:58Z" id="1203" creator="http://www.haque.co.uk">
    <title>Joana's HeartBeats</title>
    <feed>http://www.pachube.com/api/1203.xml</feed>
    <status>frozen</status>
    <description>    Joana's Heartbeat is a sensor that will be active  in  March 2009.&#13;
&#13;
This sensor  will be the core of "Heart of the World", a project developed for the Pre-Production Curriculum Unit, part of  a Sound &amp; Image Masters Degree, (Major in Digital Arts). &#13;
This project aims at highlighting the extension of the human body, through the reproduction of my heartbeat on the Internet in real time. For 31 days and 744 hours I will share my heart with  the world through Pachube, allowing each of you to do whatever you wish with it...&#13;
&#13;
I hereby invite  all interested in "using" my heart and making it an artistic intervention, to do so.&#13;
&#13;
Thank you very much :)&#13;
 </description>
    <website>http://coracaodomundo0809.wordpress.com/</website>
    <email>joanacampossilva@gmail.com</email>
    <location domain="physical" exposure="indoor" disposition="mobile">
      <name>Laborim, Vila Nova de Gaia</name>
      <lat>41.1061311580386</lat>
      <lon>-8.60770225524902</lon>
    </location>
    <data id="0">
      <tag>Coração do Mundo</tag>
      <tag>HeartBeats</tag>
      <value minValue="0.0" maxValue="218.0">81.0</value>
    </data>
  </environment>
</eeml>
}

}

