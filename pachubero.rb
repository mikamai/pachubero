require 'open-uri'
require 'net/http'
require 'rubygems'
require 'cuneiform'
require 'mockdata'

class Pachube
  PACHUBE_SERVER = "www.pachube.com"
  PACHUBE_BASE_URL = "http://#{PACHUBE_SERVER}/api/"

  attr_accessor :key

  def initialize key
    @key=key
  end

  def feed feed_no
    PachubeFeed.new(self, feed_no).refresh
  end

  def download_page mode, feed_no
    return MOCK_DATA[feed_no] if :mock == @key
    open(pachube_url(mode, feed_no)).read
  end

  def server
    "www.pachube.com"
  end

  def uri mode, feed_no
    "/api/#{feed_no}.#{mode}?key=#{@key}"
  end

private
  def pachube_url mode, feed_no
    "#{PACHUBE_BASE_URL}#{feed_no}.#{mode}?key=#{@key}"
  end
end

class PachubeFeed
  attr_reader :text
  
  def initialize pachube, feed_no
    @pachube = pachube
    @feed_no = feed_no
  end

  def refresh
    @text = @pachube.download_page :xml, @feed_no
    @feed = xml(@text).environment
  self end

  def data
    for i in 0...size 
      yield tag(i), value(i), min(i), max(i)
    end
  end

  def id
    @feed.id
  end
  
  def title
    @feed.title
  end
  
  def status
    @feed.status
  end
  
  def location
    @feed.location.name
  end
  
  def latitude
    @feed.location.lat.v
  end

  def longitude
    @feed.location.lon.v
  end

  def description
    @feed.description
  end

  def website
    @feed.website
  end

  def tag num=0
    @feed.data[num].tag.v
  rescue
    nil
  end

  def value num=0
    @feed.data[num].value.v
  rescue
    nil
  end    

  def min num=0
    @feed.data[num].value.minValue.v
  rescue
    nil
  end    

  def max num=0
    @feed.data[num].value.maxValue.v
  rescue
    nil
  end    
  
  def size
    @feed.data.size
  end

  def put(id, value)
    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<eeml xmlns=\"http://www.eeml.org/xsd/005\">
  <environment>
    <data id=\"#{id}\">
      <value>#{value}</value>
    </data>
  </environment>
</eeml>"
    client = Net::HTTP.new(@pachube.server)
    client.send_request("PUT", @pachube.uri(:xml, @feed_no), xml,
                  { 'X-PachubeApiKey' => @pachube.key,
                    'Content-Length'  => xml.length.to_s
                  })
  end

end

